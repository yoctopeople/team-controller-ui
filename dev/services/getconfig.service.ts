import {Injectable} from 'angular2/core';
import {Http} from 'angular2/http';

@Injectable()
export class ConfigService {
    constructor(private http: Http) {}

    getConfig() {
        return this.http.get('/config.json').map((data) => {
            return JSON.parse(data._body);
        });
    }
}