import {Component} from 'angular2/core';
import {ConnectionService} from '../services/connection.service';
import {CommandLineService} from "../services/commandline.service";

@Component({
    selector: 'commandline',
    templateUrl: 'dev/templates/commandline.template.html',
    providers: [ConnectionService, CommandLineService]
})

export class CommandLineComponent {
    commands: string[] = [];

    constructor(private commandLineService: CommandLineService) {
        this.commandLineService.commandAdded.subscribe(command => {
            console.log('component', command);
        });
    }
}