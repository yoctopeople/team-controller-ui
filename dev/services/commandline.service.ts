import {Injectable} from 'angular2/core';
import {Observable} from 'rxjs/Observable';
import {Subject} from "rxjs/Subject";

@Injectable()

export class CommandLineService {
    commands: string[] = [];
    commandAdded: Subject<string> = new Subject();

    add(command: string): void {
        console.log('service', command);
        if (command) {
            this.commands.push(command);
            this.commandAdded.next(command);
        }
        console.log(this.commands);
    }
}