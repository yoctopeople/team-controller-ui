import {bootstrap} from 'angular2/platform/browser';
import 'rxjs/Rx';
import {ROUTER_PROVIDERS} from 'angular2/router';
import {HashLocationStrategy} from 'angular2/src/platform/browser/location/hash_location_strategy';
import {LocationStrategy} from 'angular2/src/platform/browser/location/location_strategy';
import {provide} from 'angular2/core';
import {AppComponent} from './components/app.component';

bootstrap(
    AppComponent,
    [
        ROUTER_PROVIDERS,
        provide(LocationStrategy, {useClass: HashLocationStrategy, useValue: '/'})
    ]);