import {Component} from 'angular2/core';
import {ConfigService} from '../services/getconfig.service';
import {CommandLineService} from "../services/commandline.service";

@Component({
    selector: 'commandlist',
    templateUrl: 'dev/templates/commandlist.template.html',
    providers: [ConfigService, CommandLineService]
})

export class CommandListComponent {
    config: any;
    getConfigError: string;
    commandList: string[];

    constructor(private _configService: ConfigService, private commandLineService: CommandLineService) {
        var self = this;

        this._configService.getConfig()
            .then(
                function(config) {
                    self.config = config;
                    self.getConfigCallback();
                },
                function(error) {
                    self.getConfigError = error;
                }
            );
    }

    getConfigCallback(): void {
        this.commandList = this.config.commands[this.config.role];
    }

    checkCommand(command: string): void {
        this.commandLineService.add(command);
    }

}