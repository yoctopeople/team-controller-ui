import {Component} from 'angular2/core';
import {ROUTER_DIRECTIVES, RouteConfig, RouteParams} from 'angular2/router';
import {TeamContollerComponent} from './teamcontroller.component';

@Component({
    selector: 'app',
    templateUrl: 'dev/templates/app.template.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [RouteParams]
})

@RouteConfig([
    {path: '/:id/:role', name: 'Team controller', component: TeamContollerComponent}
])

export class AppComponent {
    constructor() {}
}