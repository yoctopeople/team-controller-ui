import {Component} from 'angular2/core';
import {HTTP_PROVIDERS} from 'angular2/http';
import {ConfigService} from '../services/getconfig.service';
import {ConnectionService} from '../services/connection.service';
import {CommandListComponent} from "./commandlist.component";
import {CommandLineComponent} from "./commandline.component";
import {RouteParams} from 'angular2/router';

@Component({
    selector: 'teamcontroller',
    templateUrl: 'dev/templates/teamcontroller.template.html',
    directives: [CommandListComponent, CommandLineComponent],
    providers: [HTTP_PROVIDERS, ConfigService, ConnectionService]
})

export class TeamContollerComponent {
    config: any;
    getConfigError: string;
    connectionService: any;
    currentActor: string = '';
    connectionSockets: any = {};
    commandsToSend: string[] = [];
    commandList: string[] = [];
    commandLog: any[] = [];
    commandLogDiv: string = '.j-command-log';
    commandWindowDiv: string = '.j-command-window';
    participants: any = {
        other: []
    };
    missionCompleted: boolean = false;
    routeParams: RouteParams;
    teamId: string;
    role: string;
    commandMap: any = {
        gameStarted: 'onGameStarted',
        commandsSet: 'onCommandSet',
        missionCompleted: 'onMissionCompleted',
        gameCompleted: 'onGameCompleted'
    };

    constructor(private configService: ConfigService,
                private connectionService: ConnectionService,
                private routeParams: RouteParams
    ) {
        this.teamId = this.routeParams.get('id');
        this.role = this.routeParams.get('role');

        this.configService.getConfig().subscribe((configData) => {
            this.config = configData;
            this.getConfigCallback();
        }, (errorData) => {
            console.error('Error parsing /config.json file', errorData);
        })
    }

    getConfigCallback(): void {
        this.connectionService.connect(this.config, this.teamId);
        this.getParticipants();

        this.connectionService.wsObservables.controlFlow.subscribe((controlFlowData) => {
            console.log(controlFlowData);
            // use controlFlowData
            var data = JSON.parse(controlFlowData.data);
            var self = this;
            //console.log(controlFlowData);

            if (data.type === 'flowControl') {
                if (self.commandMap[data.message]) self[self.commandMap[data.message]](data);
                self.getTeamStatus();
            }
        });

        this.commandList = this.config.commands[this.role];
    }

    onGameStarted(): void {
        if (!this.participants.other.length || !this.participants.current) this.getParticipants();
        this.missionCompleted = false;
    }

    onCommandSet(data: any): void {
        var self = this;
        // Setting new commands to log
        var urlTeamStatus = this.config.connections.restAPI + '/api/team/' + this.teamId;
        console.log(urlTeamStatus);
        $.getJSON(urlTeamStatus, function(dataTeamStatus) {
            console.log(dataTeamStatus);
            data.params.commands.forEach(function(command) {
                self.commandLog.push({role: self.currentActor, command: command});
            });
        });

        $(this.commandLogDiv).animate({
            scrollTop: $(this.commandLogDiv)[0].scrollHeight
        }, 'fast');
    }

    onMissionCompleted(): void {
        this.missionCompleted = true;
    }

    onGameCompleted(): void {
        if (this.commandsToSend) this.commandsToSend = [];
        if (this.commandLog) this.commandLog = [];
        if (this.participants.other.length) this.participants.other = [];
        if (this.participants.current) this.participants.current = {};
    }

    getTeamStatus(): void {
        var self = this;
        var teamStatusUrl = this.config.connections.restAPI + '/api/team/' + this.teamId;
        $.getJSON(teamStatusUrl, function(teamStatusData) {
            self.currentActor = teamStatusData.name.actor;
            console.log(self.currentActor);
        });
    }

    chooseCommand(command: string): void {
        if (command && this.role == this.currentActor) {
            this.commandsToSend.push(command);
            $(this.commandWindowDiv).animate({
                scrollTop: $(this.commandWindowDiv)[0].scrollHeight
            }, 'fast');
        }
    }

    sendCommands(): void {
        if (this.commandsToSend.length && this.role == this.currentActor) {
            var self = this;
            var urlToSend = this.config.connections.restAPI + '/api/team/' + this.teamId + '/' + this.role + '/commands';

            $.ajax({
                beforeSend: function(xhrObj) {
                    xhrObj.setRequestHeader('Content-Type', 'application/json');
                    xhrObj.setRequestHeader('Accept', 'application/json');
                },
                type: 'POST',
                dataType: 'text',
                url: urlToSend,
                data: JSON.stringify({commands: self.commandsToSend.join('')})
            }).done(function () {
                self.commandsToSend = [];
            }).error(function (_, status, err) {
                console.log("Error! Status: " + status + " Err: " + err);
            });
        }
    }

    deleteCommand(i: any): void {
        if (i >= 0) this.commandsToSend.splice(i, 1);
    }

    getParticipants(): void {
        var self = this;
        var participantsUrl = this.config.connections.restAPI + '/api/team/' + this.teamId + '/members';
        $.getJSON(participantsUrl, function(participantsData) {
            console.log(participantsData);
            participantsData.forEach(function(item) {
                if (item.role === self.role) self.participants.current = {firstName: item.firstName, lastName: item.lastName};
                else self.participants.other.push({firstName: item.firstName, lastName: item.lastName, role: item.role});
            });
            console.log(self.participants);
        });
    }

}