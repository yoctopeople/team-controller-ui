import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ConnectionService {
    error: any;
    connections: any = {};
    wsObservables: any = {};

    constructor() {

    }

    connect(config: any, teamId: string) {
        var self = this;

        // Connect to controlFlow server
        this.wsObservables.controlFlow = Observable.create((observer) => {
            self.connections.controlFlow = new WebSocket(config.connections.controlFlow);

            self.connections.controlFlow.onopen = function() {
                var initMsg = {
                    name: 'init',
                    teamId: teamId
                };

                self.connections.controlFlow.send(JSON.stringify(initMsg));
            };

            self.connections.controlFlow.onmessage = function(data) {
                observer.next(data);
            };

            return () => {
                self.connections.controlFlow.close();
            };
        }).share();
    }

}